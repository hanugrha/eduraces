<?php 
	class Database
	{
		public static $connection;

		public function connect()
		{
			if(!isset(self::$connection))
			{
				$config = parse_ini_file("db_config.ini");

				self::$connection = new mysqli($config["hostname"], $config["username"], $config["password"], $config["dbname"]);
			}

			if(self::$connection === false)
			{
				return false;
			}

			return self::$connection;
		}

		public function query($query)
		{
			$connection = $this->connect();
			$result = $connection->query($query);
			$connection->close();

			return $result;
		}

		public function select($query)
		{
			$rows = array();
			$result = $this->query($query);
			if($result === false)
			{
				return false;
			}

			while($row = $result->fetch_assoc())
			{
				$rows[] = $row;
			}

			return $rows;
		}
	}
?>