﻿using UnityEngine;
using System;
using System.Collections;

public class RacerScript : MonoBehaviour 
{
	
	public delegate void RacerEventHandler (object sender, EventArgs e = null);

	public static event RacerEventHandler GainScore;
	public static event RacerEventHandler WinRace;

	public string Username { get; set; }
	public int Score { get; set; }
	public int Position { get; set; }
	private bool isActivePlayer;
	public bool IsActivePlayer { get { return isActivePlayer; } }
	public bool IsPlayer { get; set; }

	// Use this for initialization
	void Start () 
	{
		Score = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void InitializeRacer (int position, string username, bool isActivePlayer = true, bool isPlayer = true)
	{
		Position = position;
		Username = username;
		this.isActivePlayer = isActivePlayer;
		IsPlayer = isPlayer;
	}

	public void OnGainedScore ()
	{
		if (Score < PlayerPrefs.GetInt ("CourseLength") * ProblemManager.SCORE_MULTIPLIER) 
		{
			GainScore (this);
		}
		else 
		{
			WinRace (this);
		}
	}
}
