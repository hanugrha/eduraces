﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AvatarScript : MonoBehaviour 
{

	const string AvatarURL = "http://localhost/eduraces/avatar.php";
	const string SuccessMessage = "Success";

	public Image avatarPreview;

	private int chosenAvatar = 0;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void SetAvatar (int selectedAvatar)
	{
		chosenAvatar = selectedAvatar;
	}

	public void SetAvatarPreview (Sprite selectedAvatar)
	{
		avatarPreview.overrideSprite = selectedAvatar;
	}
	
	public void ChangeAvatar ()
	{
		StartCoroutine ("ChangeAvatarCoroutine");
	}
	
	public IEnumerator ChangeAvatarCoroutine ()
	{
		WWWForm avatarForm = new WWWForm ();
		avatarForm.AddField ("username", "admin");
		avatarForm.AddField ("avatar", chosenAvatar);
		
		WWW avatarLink = new WWW (AvatarURL, avatarForm);
		yield return avatarLink;
		if (!string.IsNullOrEmpty(avatarLink.text)) {
			Debug.Log (avatarLink.text);
		}
	}
}
