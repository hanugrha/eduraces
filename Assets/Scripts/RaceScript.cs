﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class RaceScript : MonoBehaviour 
{
	
	public const int MAX_COURSE_LENGTH = 30;

	public delegate void RaceEventHandler ();

	public static event RaceEventHandler OnStarted;

	public static float GameTime = 0;

	public GameObject waitingPanel;
	public GameObject problemPanel;
	public Text timeText;

	private bool isStarted = false;
	private bool isRacing = false;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!isStarted) 
		{
			if (Input.GetKeyUp (KeyCode.Return)) 
			{
				waitingPanel.SetActive (false);
				problemPanel.SetActive (true);
				StartRace ();
			}
		} 

		if (isRacing) 
		{
			UpdateTime ();
		}
	}

	void OnEnable ()
	{
		RacerScript.WinRace += EndRace;
	}

	void OnDisable ()
	{
		RacerScript.WinRace -= EndRace;
	}

	public void StartRace ()
	{
		Animator enemyAnimator = FindObjectOfType (typeof(Animator)) as Animator;
		enemyAnimator.speed = MAX_COURSE_LENGTH / PlayerPrefs.GetInt ("CourseLength");
		enemyAnimator.enabled = true;

		EnemyScript enemy = FindObjectOfType (typeof(EnemyScript)) as EnemyScript;
		enemy.StartGainScore ();

		OnStarted ();

		isRacing = true;
	}

	public void StopRace ()
	{
		Animator enemyAnimator = FindObjectOfType (typeof(Animator)) as Animator;
		enemyAnimator.enabled = false;

		EnemyScript enemy = FindObjectOfType (typeof(EnemyScript)) as EnemyScript;
		enemy.StopGainScore ();

		isRacing = false;
	}

	public void UpdateTime ()
	{
		GameTime += Time.deltaTime;

		float seconds = Mathf.Floor (GameTime % 60);
		float minutes = Mathf.Floor (GameTime / 60);

		string secondsString = ((seconds < 10) ? "0" : "") + seconds.ToString ();
		string minutesString = ((minutes < 10) ? "0" : "") + minutes.ToString ();

		timeText.text = minutesString + ":" + secondsString;
	}

	public void EndRace (object sender, EventArgs e = null)
	{
		StopRace ();
	}
}
