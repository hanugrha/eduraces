﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GlobalScript : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public void ChangeScene (string nextScene) 
	{
		SceneManager.LoadScene (nextScene);
	}

	public void SetDifficulty (string difficulty) 
	{
		PlayerPrefs.SetString ("Difficulty", difficulty);
	}

	public void SetCourseLength (int length) 
	{
		PlayerPrefs.SetInt ("CourseLength", length);
	}

	public void Exit () 
	{
		Application.Quit ();
	}


}
