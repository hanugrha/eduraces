﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SignupScript : MonoBehaviour 
{

	const string SignupURL = "http://localhost/eduraces/signup.php";
	const string ValidInputMessage = "Valid";
	const string SuccessMessage = "Success";
	const int PasswordMinLength = 8;

	public readonly string[] SignupFormInputs = { "Firstname", "Lastname", "Email", "Username", "Password", "Gender"};
	public readonly int[] SignupFormInputsMaxLength = { 0, 50, 254, 20, 16, 0};
	public InputField[] formInputs;
	public InputField repasswordInput;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public string CheckInputs ()
	{
		for (int i = 0; i < formInputs.Length; i++)
		{
			if (string.IsNullOrEmpty (formInputs [i].text)) 
			{
				return SignupFormInputs [i] + " field must be filled.";
			}
				
			if (i == 1) 
			{
				string name = formInputs [i - 1].text + " " + formInputs [i].text + " ";
				if (name.Length > SignupFormInputsMaxLength [i]) 
				{
					return "Your name must be less than " + SignupFormInputsMaxLength [i] + " letters.";
				}
			} 
			else if (i == 4) 
			{
				if (formInputs [i].text.Length < PasswordMinLength) 
				{
					return "Your " + SignupFormInputs [i].ToLower () + " must be more than " + PasswordMinLength + " characters.";
				}
			}
			else if (i > 0 && i < formInputs.Length - 1) 
			{
				if (formInputs [i].text.Length > SignupFormInputsMaxLength [i]) 
				{
					return "Your " + SignupFormInputs [i].ToLower () + " must be less than " + SignupFormInputsMaxLength [i] + " characters.";
				}
			}
		}
			
		if (!formInputs [4].text.Equals (repasswordInput.text)) 
		{
			return "Password don't match.";
		}

		return ValidInputMessage;
	}
	
	public void Signup ()
	{
		string message = CheckInputs ();

		if (message.Equals (ValidInputMessage)) 
		{
			Debug.Log ("Clear");
			//StartCoroutine ("SignupCoroutine");
		} 
		else 
		{
			Debug.Log (message);
		}
	}
	
	public IEnumerator SignupCoroutine ()
	{
		WWWForm signupForm = new WWWForm ();
		for (int i = 0; i < SignupFormInputs.Length; i++)
		{
			signupForm.AddField (SignupFormInputs[i].ToLower (), formInputs[i].text);
		}
		
		WWW signupLink = new WWW (SignupURL, signupForm);
		yield return signupLink;
		if (!string.IsNullOrEmpty(signupLink.text)) {
			Debug.Log (signupLink.text);
		}
	}
}
