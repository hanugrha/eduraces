﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class RacerManager : MonoBehaviour 
{

	public GameObject enemyPrefab;
	public GameObject[] racers;
	public Text[] positionsText;

	// Use this for initialization
	void Start () 
	{
		string playerName = PlayerPrefs.GetString ("ActiveUser");

		// Single Player
		racers[0].GetComponent<RacerScript> ().InitializeRacer (0, (playerName != null) ? playerName : "Player", true, true);
		InitializeEnemy ();
		SetPositionsText ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnEnable ()
	{
		RacerScript.GainScore += UpdatePositions;

		RacerScript.WinRace += EndRace;
	}

	void OnDisable ()
	{
		RacerScript.GainScore -= UpdatePositions;

		RacerScript.WinRace -= EndRace;
	}

	public void InitializeEnemy ()
	{
		GameObject racer = Instantiate (enemyPrefab) as GameObject;
		racer.name = "Enemy";
		racer.transform.SetParent (GameObject.Find ("Progress Bar").transform);
		racer.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (35, -50);
		racer.GetComponent<RectTransform> ().localScale = Vector3.one;
		racer.GetComponent<RacerScript> ().InitializeRacer (1, "EduBoy", false, false);

		racers[1] = racer;
	}

	public void SetPositionsText ()
	{
		for (int i = 0; i < positionsText.Length; i++) 
		{
			positionsText [i].text = "";

			if (i < racers.Length) 
			{
				positionsText [i].text = racers [i].GetComponent<RacerScript> ().Username;
			}
		}
	}

	public void UpdatePositions (object sender, EventArgs e)
	{
		RacerScript racer = ((RacerScript)sender);
		RacerScript racerAhead;
		GameObject temp;

		for (int i = racer.Position; i > 0; i--) 
		{
			racerAhead = racers [i - 1].GetComponent<RacerScript> ();

			if (racer.Score >= racerAhead.Score) 
			{
				temp = racers [i];
				racers [i] = racers [i - 1];
				racers [i].GetComponent<RacerScript> ().Position = i;
				racers [i - 1] = temp;
				racers [i - 1].GetComponent<RacerScript> ().Position = i - 1;
			}
		}

		SetPositionsText ();
	}

	public void EndRace (object sender, EventArgs e = null)
	{
		foreach (GameObject racer in racers) 
		{
			PlayerScript player = racer.GetComponent<PlayerScript> ();
			if (player != null) 
			{
				player.EndRace ();
			}
		}
	}
}
