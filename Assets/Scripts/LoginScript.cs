﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoginScript : MonoBehaviour 
{

	const string LoginURL = "http://localhost/eduraces/login.php";
	const string SuccessMessage = "Success";

	public InputField usernameField;
	public InputField passwordField;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public void Login ()
	{
		StartCoroutine ("LoginCoroutine");
	}

	public IEnumerator LoginCoroutine ()
	{
		WWWForm loginForm = new WWWForm ();
		loginForm.AddField ("username", usernameField.text);
		loginForm.AddField ("password", passwordField.text);

		WWW loginLink = new WWW (LoginURL, loginForm);
		yield return loginLink;
		if (!string.IsNullOrEmpty(loginLink.text)) 
		{
			string[] message = loginLink.text.Split (new char[] {':'});

			if (message[0].Equals(SuccessMessage))
			{
				PlayerPrefs.SetString ("Username", message [1]);
				SceneManager.LoadScene (1);
			}
			else
			{
				Debug.Log (message[1]);
			}
		}
	}
}
