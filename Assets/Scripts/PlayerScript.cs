﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class PlayerScript : MonoBehaviour 
{

	public class PlayerEventArgs : EventArgs
	{

		private string answer;

		public string Answer { get { return answer; } }

		public PlayerEventArgs (string answer)
		{
			this.answer = answer;
		}
	}
		
	public delegate void PlayerEventHandler (object sender, PlayerEventArgs e = null);

	public static event PlayerEventHandler OnRequestedProblem;
	public static event PlayerEventHandler OnAnsweredProblem;

	public GameObject playerCar;
	public Text questionText;
	public Text [] answersText;
	public float raceDistance = 630;
	public Sprite[] trophies;
	public GameObject endPanel;

	private int currentProblemIndex = 0;
	public int CurrentProblemIndex { get { return currentProblemIndex; } }

	// Use this for initialization
	void Start () 
	{

	}

	// Update is called once per frame
	void Update () 
	{

	}

	void OnEnable ()
	{
		RaceScript.OnStarted += RequestProblem;
	}

	void OnDisable ()
	{
		RaceScript.OnStarted -= RequestProblem;
	}

	public void RequestProblem ()
	{
		OnRequestedProblem (this);
	}

	public void SetProblemText (string question, string[] answers)
	{
		questionText.text = question;
		for (int i = 0; i < answersText.Length; i++) 
		{
			answersText [i].text = answers [i];
		}
	}

	public void AnswerProblem (string answer)
	{
		PlayerEventArgs e = new PlayerEventArgs (answer);
		OnAnsweredProblem (this, e);
	}

	public void AnswerCorrectly ()
	{
		MoveCar ();

		GetComponent<RacerScript> ().Score += ProblemManager.SCORE_MULTIPLIER;
		GetComponent<RacerScript> ().OnGainedScore ();

		currentProblemIndex++;
		RequestProblem ();
	}

	public void MoveCar ()
	{
		((RectTransform)playerCar.transform).anchoredPosition += new Vector2 ((float)(raceDistance / PlayerPrefs.GetInt ("CourseLength")), 0);
	}

	public void EndRace ()
	{
		RacerScript racer = GetComponent<RacerScript> ();
		Transform trophy = endPanel.transform.Find ("Trophy");
		Text endTitle = endPanel.transform.Find ("Title").GetComponent<Text> ();

		trophy.GetComponent<Image> ().sprite = trophies [racer.Position];
		endTitle.text = racer.Position == 0 ? "SELAMAT" : "COBA LAGI";

		endPanel.transform.Find ("Effect").gameObject.SetActive (racer.Position == 0 ? true : false);
		endPanel.SetActive (true);
	}
}
