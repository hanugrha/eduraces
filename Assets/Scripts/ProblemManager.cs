﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;

public class ProblemManager : MonoBehaviour 
{

	public const int SCORE_MULTIPLIER = 10;

	public class Problem
	{
		private string question;
		private string answer1;
		private string answer2;
		private string answer3;
		private string answer4;
		private string correctAnswer;

		public Problem (string question, string answer1, string answer2, string answer3, string answer4, string correctAnswer)
		{
			this.question = question;
			this.answer1 = answer1;
			this.answer2 = answer2;
			this.answer3 = answer3;
			this.answer4 = answer4;
			this.correctAnswer = correctAnswer;
		}

		public bool CheckAnswer (string answer)
		{
			return answer.Equals(correctAnswer);
		}

		public void GetQuestionAnswers (out string question, out string[] answers)
		{
			string[] temp = new string[4];

			question = this.question;
			temp[0] = answer1;
			temp[1] = answer2;
			temp[2] = answer3;
			temp[3] = answer4;
			answers = temp;
		}
	}

	public TextAsset source;

	private List<Problem> problems = new List<Problem> ();

	// Use this for initialization
	void Start ()
	{
		// Initialize Problems
		LoadProblems ();
		Shuffle ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnEnable ()
	{
		PlayerScript.OnRequestedProblem += GetNewProblem;

		PlayerScript.OnAnsweredProblem += CheckAnswer;
		PlayerScript.OnAnsweredProblem += GetNewProblem;
	}

	void OnDisable ()
	{
		PlayerScript.OnRequestedProblem -= GetNewProblem;

		PlayerScript.OnAnsweredProblem -= CheckAnswer;
		PlayerScript.OnAnsweredProblem -= GetNewProblem;
	}

	public void LoadProblems ()
	{
		using (XmlReader reader = XmlReader.Create (new StringReader (source.text)))
		{
			while (reader.ReadToFollowing ("question"))
			{
				string question = reader.ReadElementContentAsString ();

				reader.ReadToFollowing ("answer1");
				string answer1 = reader.ReadElementContentAsString ();

				reader.ReadToFollowing ("answer2");
				string answer2 = reader.ReadElementContentAsString ();

				reader.ReadToFollowing ("answer3");
				string answer3 = reader.ReadElementContentAsString ();

				reader.ReadToFollowing ("answer4");
				string answer4 = reader.ReadElementContentAsString ();

				reader.ReadToFollowing ("correctanswer");
				string correctAnswer = reader.ReadElementContentAsString ();

				problems.Add (new Problem (question, answer1, answer2, answer3, answer4, correctAnswer));
			}
		}
	}

	public void Shuffle ()
	{ 
		int n = problems.Count;  
		while (n > 1) {  
			n--;  
			int k = Random.Range (0, n + 1);  
			Problem problem = problems[k];  
			problems[k] = problems[n];  
			problems[n] = problem;  
		}
	}

	public void GetNewProblem (object sender, PlayerScript.PlayerEventArgs e)
	{
		string question;
		string[] answers;

		problems [((PlayerScript)sender).CurrentProblemIndex].GetQuestionAnswers (out question, out answers);

		((PlayerScript)sender).SetProblemText (question, answers);
	}

	public void CheckAnswer (object sender, PlayerScript.PlayerEventArgs e)
	{
		if (problems [((PlayerScript)sender).CurrentProblemIndex].CheckAnswer (e.Answer)) 
		{
			((PlayerScript)sender).AnswerCorrectly ();
		}
	}
}
