﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DropdownScript : MonoBehaviour 
{

	public GameObject optionPanel;
	public InputField selectedValue;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ExpandOptions ()
	{
		optionPanel.SetActive (!optionPanel.activeSelf);

	}

	public void ChangeValue (Text selectedOption)
	{
		selectedValue.text = selectedOption.text;
		optionPanel.SetActive (!optionPanel.activeSelf);
	}
}
