﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour
{

	private bool isRacing = false;

	// Use this for initialization
	void Start () 
	{
		Animator animator = gameObject.GetComponent<Animator> ();
		animator.speed = RaceScript.MAX_COURSE_LENGTH / PlayerPrefs.GetInt ("CourseLength");
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public IEnumerator GainScore ()
	{
		while (isRacing) 
		{
			yield return new WaitForSeconds (1f);

			GetComponent<RacerScript> ().Score = ProblemManager.SCORE_MULTIPLIER * (int)RaceScript.GameTime / 2;
			GetComponent<RacerScript> ().OnGainedScore ();
		}
	}

	public void StartGainScore ()
	{
		isRacing = true;
		StartCoroutine (GainScore ());
	}

	public void StopGainScore ()
	{
		isRacing = false;
		StopCoroutine (GainScore ());
	}
}
